const smalltalk = require('smalltalk');
const $ = require('jquery');
$.put = function(url, data, success, complete) {
    $.ajax(
        {
            url: url,
            type: "PUT",
            data: JSON.stringify(data),
            success: success,
            complete: complete
        });
};

var boards = require('./boards.js');
var actions = require('./actions.js');
var devices = require('./devices.js');
var move = require('./move.js');

boards.host = actions.host = devices.host = move.host = "localhost";

var current = null;


var entries = {
    "Boards": boards,
    "Devices": devices,
    "Actions": actions,
    "Move": move,
};

var lis = {};

function activate(entry) {
    if (current != null) {
        current.stop();
    }
    for(var li in lis) {
        lis[li].removeClass('selected');
    }
    lis[entry].addClass('selected');
    current = entries[entry];
    current.show();
}

function setup_menu() {
    var menu = $('nav');

    var lst = $('<ul/>').appendTo(menu);

    $.each(entries, function(entry) {
        var li = $('<li/>')
            .appendTo(lst)
            .text(entry)
            .on('click', function() {
                activate(entry);
            });
        lis[entry] = li;
    });
}

setup_menu();

smalltalk.prompt('Question', "What's the IP address?", 'crapobot-eth')
.then((value) => {
    console.log("GOT", value);
    boards.host = actions.host = devices.host = move.host = value;
    activate("Move");
});
