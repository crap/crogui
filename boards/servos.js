const $ = require("jquery");
const base = require("./base.js");

class ServosBoard extends base.Board {

    init(data) {
        super.init(data);

        var table = $("<table/>").appendTo(this.content_div);
        $("<tr/>")
            .appendTo(table)
            .append("<th>ID</th><th>position</th><th>ON</th>");

        for (let i=0; i<6; i++) {
            let label = $("<label/>")
                .text(i);
            let valrange = $("<input/>")
                .attr("type", "range")
                .attr("min", 0)
                .attr("max", 100);

            let val = $("<label/>");

            let chk = $("<input/>")
                .attr("type", "checkbox");

            let row = $("<tr/>");
            row.append($("<td/>").append(label));
            row.append($("<td/>").append(valrange).append(val));
            row.append($("<td/>").append(chk));

            row.appendTo(table);

            let servo = new Servo(
                this.url,
                i,
                data.servos[i].active,
                data.servos[i].position,
                valrange,
                val,
                chk);
            servo.redraw();
        }
    }
}

class Servo {
    constructor(base_url, servo_id, active, position, valrange, valtxt, chk) {
        // Storage
        this.url = base_url + "/" + servo_id;

        // Real data to send
        this.active = active;
        this.position = parseFloat(position.toFixed(1));

        // Input Elements
        this.valrange = valrange;
        this.valtxt = valtxt;
        this.chk = chk;

        this._ready = true;

        var that = this;

        this.valrange.on("change", function(evt){that.input_changed(evt);});
        this.valrange.on("input", function(evt){that.input_changed(evt);});
        this.chk.on("click", function(evt) {
            that.active = evt.target.checked;
            that.update();
            that.redraw();
        });
    }

    update() {
        if (this._ready) {
            this._ready = false;
            let that = this;
            $.put(this.url,
                {
                    active: this.active,
                    position: this.position,
                    duration: 0
                },
                null,
                function() {
                    that._ready = true;
                });
        }
    };

    redraw() {
        this.valrange.val(this.position);
        this.valtxt.text(this.position);
        this.chk.attr('checked', this.active);
    };

    input_changed(evt) {
        this.position = parseInt(evt.target.value);
        this.update();
        this.redraw();
    };
}

exports.Board = ServosBoard;
