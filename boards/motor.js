const $ = require("jquery");
const base = require("./base.js");

class MotorBoard extends base.Board {

    init(data) {
        console.log("Motor Init data", data);
        super.init(data);

        data = Object.assign({motor: {pwm: 0}}, data);

        this._pwm = parseFloat(data.motor.pwm.toFixed(1));
        this._ready = true;

        let div = $('<div/>').appendTo(this.content_div);
        let txt = $("<label/>")
            .text("PWM:")
            .appendTo(div);
        this.valrange = $("<input/>")
            .attr("type", "range")
            .attr("min", -100)
            .attr("max", 100)
            .val(this._pwm)
            .appendTo(div);
        this.val = $("<label/>")
            .text(this._pwm)
            .appendTo(div);

        var that = this;

        this.valrange.on("change", function(evt){that.input_changed(evt);});
        this.valrange.on("input", function(evt){that.input_changed(evt);});


        div = $('<div/>').appendTo(this.content_div);
        $("<label/>")
            .text("Current:")
            .appendTo(div);
        $("<label/>")
            .text(data.motor.current.toFixed(1))
            .appendTo(div);
    }

    input_changed(evt) {
        this._pwm = parseInt(evt.target.value);
        this.update();
        this.redraw();
    };

    update() {
        if (this._ready) {
            this._ready = false;
            var that = this;
            $.put(this.url + '/pwm', this._pwm, undefined, function() {
                that._ready = true;
            });
        }
    };

    redraw() {
        this.valrange.val(this._pwm);
        this.val.text(this._pwm);
    };

}

exports.Board = MotorBoard;
