const $ = require("jquery");
const paper = require('paper');

class Move {
    constructor(content) {
        this.url = 'http://' + exports.host + ':8080/api/move';
        this.div = $('<div/>')
            .addClass('move')
            .html(' \
            <canvas id="move-canvas" width="3000" height="2000">HERE MAP</canvas> \
            <div id="move-controls"> \
                <span>Color:</span> \
                <button class="blue">Blue</button> \
                <button class="yellow">Yellow</button> \
                <span>Move:</span> \
                <button class="movemode">line</button> \
                <button class="movego">Go!</button> \
                <span>Test:</span> \
                <span class="small"><button class="testangle">0</button>deg</span> \
                <span class="small"><button class="testdist">10</button>cm</span> \
                <button class="testgo">Go!</button> \
            </div>')
            .appendTo(content);

        paper.setup($('#move-canvas')[0]);


        this.modes = ["line", "dijkstra", "avoid", "point"];
        this.angles = [0, 45, 90, 135, 180, -135, -90, -45];
        this.dists = [0, 5, 10, 20, 50, 100];

        // LOADING
        this.ratio = paper.view.size.width / 3;
        console.log("Ratio", this.ratio, paper.view.size.width);

        this.layers = {
            obstacles: new paper.Layer({name: 'Obstacles'}),
            path: new paper.Layer({name: 'Path'}),
            robot: new paper.Layer({name: 'Robot'}),
            target: new paper.Layer({name: 'Target'})
        };

        this.obstacles = [];

        this.rob = null;

        $.get(this.url, (data) => {
            console.log("GET", data);

            // Create Robot
            this.rob = new Robot(
                this,
                this.url + "/reset",
                data.position,
                0.2);
        });


        this.tar = new Target(this, this.url + "/to");
        this.tar.onPositionSelected = (pos) => {this.testTarget(pos);};

        this.path = new Path(this);

        paper.view.draw();

        // Fetch HTML elements
        this.$bluebtn = this.div.find("button.blue");
        this.$yellowbtn = this.div.find("button.yellow");
        this.$gobtn = this.div.find("button.movego");
        this.$modebtn = this.div.find("button.movemode");

        this.$bluebtn.on('click', this.setColor('blue'));
        this.$yellowbtn.on('click', this.setColor('yellow'));
        this.$gobtn.on('click' , () => {this.go();});

        this.$modebtn.on('click', () => {this.toggleMode();});
        this.$modebtn.html(this.modes[1]);

        // Simple Move HTML elements
        this.$btnangle = this.div.find("button.testangle");
        this.$btndist = this.div.find("button.testdist");
        this.$btngo = this.div.find("button.testgo");

        this.$btnangle.on('click', () => {this.toggleAngle();});
        this.$btndist.on('click', () => {this.toggleDist();});
        this.$btngo.on('click', () => {this.testGo();});

        // Start WS
        var ws_url = this.url.replace("http", "ws") + "/events";
        console.log("Opening WS", ws_url);
        this.ws = new WebSocket(ws_url);

        this.ws.onmessage = (evt) => {
            var data = JSON.parse(evt.data);

            this.rob.x = data.position.x;
            this.rob.y = data.position.y;
            this.rob.theta = data.position.theta;
            this.rob.update();

            this.path.reset(data.position, data.targets, 0.2);

            for (let o of this.obstacles) {
                console.log("o", o);
                o.p.remove();
            }

            this.obstacles = [];

            console.log("robots:", data.robobstacles);
            for (let o of data.robobstacles) {
                this.obstacles.push(
                    new Obstacle(this, o)
                );
            }
        };
    }

    stop() {
        this.ws.close();
    }

    toValue(val) {
        return val * this.ratio;
    }
    toPoint(obj) {
        return new paper.Point(this.toValue(obj.y), this.toValue(obj.x));
    }
    fromValue(val) {
        return val / this.ratio;
    }
    fromPoint(obj) {
        return {
            x: this.fromValue(obj.y),
            y: this.fromValue(obj.x),
            theta: [0, obj.theta]['theta' in obj]
        };
    }

    setColor(color) {
        return (evt) => {
            console.log ("Color " + color);
            let url = this.url.replace("move", "color") + "/" + color;
            $.put(
                url,
                {},
                (data) => {
                    console.log("Color set to ", color);
                }
            );
        };
    }

    toggleMode() {
        let ix = this.modes.indexOf(this.$modebtn.text());
        ix = (ix + 1) % this.modes.length;

        this.$modebtn.text(this.modes[ix]);
    }

    toggleAngle() {
        let ix = this.angles.indexOf(parseInt(this.$btnangle.text()));
        ix = (ix + 1) % this.angles.length;

        this.$btnangle.text(this.angles[ix]);
    }

    toggleDist() {
        let ix = this.dists.indexOf(parseInt(this.$btndist.text()));
        ix = (ix + 1) % this.dists.length;

        this.$btndist.text(this.dists[ix]);
    }

    testGo() {
        console.log("TESTGO");
        let d = parseFloat( this.$btndist.text() ) / 100;
        let a = parseFloat( this.$btnangle.text() ) / 180 * Math.PI;

        let dx, dy, dth;

        if (d != 0) {
            dx = Math.cos(a) * d;
            dy = Math.sin(a) * d;
            dth = 0;
        } else {
            dx = 0;
            dy = 0;
            dth = a;
        }

        let pos = {
            x: ( this.rob.x ) + dx,
            y: ( this.rob.y ) + dy,
            theta: ( this.rob.theta ) + dth,
        };

        console.log("Log Go to ", pos);
        this.tar.setPos(pos, this.$modebtn.text(), false, true);
    }

    testTarget() {
        let pos = {
            x: this.tar.x,
            y: this.tar.y,
            theta: this.rob.theta
        };
        console.log("Test Go to ", pos);
        this.tar.setPos(pos, this.$modebtn.text(), true, true);
    }

    go() {
        let pos = {
            x: this.tar.x,
            y: this.tar.y,
            theta: this.rob.theta
        };

        console.log("Go to ", pos);
        this.tar.setPos(pos, this.$modebtn.text());
    }
}


class Robot {
    constructor(drawing, url, pos, radius) {
        this.drawing = drawing;
        this.url = url;
        this.x = pos.x;
        this.y = pos.y;
        this.theta = pos.theta;

        this.drawing.layers.robot.activate();

        let center = this.drawing.toPoint(this);

        console.log("paper", paper);
        let pathData = "m 445.82597,52.031284 198.41233,0.268008 254.91651,255.139848 1.56336,77.89985 c 0,0 -13.57887,57.17419 -14.65089,82.1879 -1.07201,25.01371 7.50412,113.27638 30.01645,153.29831 22.51235,40.02194 27.37197,46.19486 31.16213,52.95396 3.79016,6.7591 2.33728,13.13918 2.33728,13.13918 l 0,25.83619 -12.06531,39.67023 -785.19265,0.0632 -11.93898,-39.54389 -0.12633,-29.56317 c 0,0 0.82119,-6.63276 4.10599,-13.32869 3.2848,-6.69594 21.66702,-30.1317 23.49893,-38.65953 1.8319,-8.52784 11.74946,-20.02462 13.13918,-28.17345 1.38973,-8.14881 12.82334,-35.31156 14.59208,-46.11348 1.76874,-10.80192 8.02248,-41.75481 7.76981,-59.94753 -0.25268,-18.19272 -2.27409,-71.76017 -10.10707,-93.49036 -7.83298,-21.73019 -2.52677,-39.92291 -2.52677,-39.92291 l -0.1895,-56.41005 z";
        let shape = new paper.Path(pathData);

                // 1px for 1mm
        let scale = this.drawing.ratio*0.4/1000;
        console.log("SCALE", scale, "ratio", this.drawing.ratio);
        shape.scale(scale);
        this.p = shape;
        this.p.position = center;

        this.prevAngle = 0;
        this.p.rotate(this.prevAngle);

        this.p.fillColor = 'gold';
        this.p.strokeColor = 'black';
        this.p.strokeWidth = this.drawing.toValue(0.005);

        this.p.onMouseDown = (evt) => {this.showText(evt);};
        this.p.onMouseDrag = (evt) => {this.move(evt);};
        this.p.onMouseUp = (evt) => {this.apply(evt);};

        paper.view.draw();

        this.text = null;

        this.updateAllowed = true;
        // this.listen-to  (this.model, "new-pos" , this.update);
    }

    update() {
        if (!this.updateAllowed) {
            return;
        }

        let pt =  this.drawing.toPoint({
            x: this.x,
            y: this.y
        });

        this.p.position = pt;

        let angle =  this.theta / Math.PI * -180;
        let diff = angle - this.prevAngle;

        this.p.rotate(diff);
        this.prevAngle = angle;
    }

    showText(evt) {
        this.updateAllowed = false;
        this.drawing.layers.robot.activate();

        if (this.text) {
            this.text.remove();
            this.text = null;
        }
        this.text = new paper.PointText(
            this.drawing.toPoint({x:0.2, y:1.5})
        );
        this.text.justification = 'center';
        this.text.fillColor = 'white';
        this.text.fontSize = this.drawing.toValue(0.1);
        this.text.fontWeight = 'bold';
    }

    move(evt) {
        this.p.position = evt.point;

        let pt = this.drawing.fromPoint(evt.point);
        this.text.content =
            "Robot: " + pt.x.toFixed(2) + "/" + pt.y.toFixed(2);
    }

    apply(evt) {
        this.updateAllowed = true;
        let pos = this.drawing.fromPoint(evt.point);
        console.log("Robot reset", pos);
        this.text.remove();

        // Save
        this.x = pos.x;
        this.y = pos.y;

        let data = {
            x: this.x,
            y: this.y,
            theta: this.theta
        };
        console.log("Putting ", data);

        $.put(this.url, data);
    }
}

class Target {
    constructor(drawing, url) {
        this.drawing = drawing;
        this.url = url;
        console.log("Target URL " + this.url);

        this.drawing.layers.target.activate();

        this.x = 1;
        this.y = 1;
        this.theta = 0;

        let pt = this.drawing.toPoint(this);
        let d = this.drawing.toPoint({x: 0.1, y:0});
        let circ = new paper.Path.Circle(pt, this.drawing.toValue(0.08));
        let cross = new paper.Path();
        cross.add(pt);
        cross.add(pt.add(d));
        cross.add(pt.subtract(d));
        cross.add(pt);
        d = d.rotate(90);
        cross.add(pt.add(d));
        cross.add(pt.subtract(d));
        this.p = new paper.CompoundPath({
            children: [circ, cross]
        });

        this.p.fillColor = '#A0A0A0';
        this.p.fillColor .alpha = 0.6;
        this.p.strokeColor = 'black';
        this.p.strokeWidth = this.drawing.toValue(0.01);

        paper.view.draw();

        this.text = null;
        this.onPositionSelected = null;

        this.p.onMouseDown = (evt) => {this.showText(evt);};
        this.p.onMouseDrag = (evt) => {this.move(evt);};
        this.p.onMouseUp = (evt) => {this.apply(evt);};
    }

    showText(evt) {
        this.drawing.layers.target.activate();
        if (this.text) {
            this.text.remove();
            this.text = null;
        }

        this.text = new paper.PointText(
            this.drawing.toPoint({x:0.2, y:1.5})
        );
        this.text.justification = 'center';
        this.text.fillColor = 'white';
        this.text.fontSize = this.drawing.toValue(0.1);
        this.text.fontWeight = 'bold';

        this.move(evt);
    }

    move(evt) {
        this.p.position = evt.point;

        let pt = this.drawing.fromPoint(evt.point);
        this.text.content =
            "Target: " + pt.x.toFixed(2)  + "/" + pt.y.toFixed(2);

        this.x = pt.x;
        this.y = pt.y;
    }

    apply(evt) {
        this.text.remove();
        if (this.onPositionSelected) {
            this.onPositionSelected(this);
        }
    }

    force(p) {
        this.p.position = this.drawing.toPoint(p);
        paper.view.draw();
    }

    setPos(target, mode, simulate=false, log=false) {
        let data = {
            target: target,
            mode: mode,
            simulate: simulate,
            log: log
        };
        $.put(this.url, data);
    }
}

class Obstacle {
    constructor(drawing, data) {
        this.drawing = drawing;
        this.drawing.layers.obstacles.activate();

        this.x = data.center.x;
        this.y = data.center.y;
        this.theta = 0;

        let pt = this.drawing.toPoint(this);
        this.p = new paper.Path.Circle(pt, this.drawing.toValue(data.radius));
        this.p.fillColor = '#A0A0A0';
        this.p.fillColor.alpha = 0.6;
        this.p.strokeColor = 'red';
        this.p.strokeWidth = this.drawing.toValue(0.01);

        paper.view.draw();
    }
}

class Path {

    constructor(drawing) {
        this.drawing = drawing;

        drawing.layers.path.activate();

        this.g = new paper.Group();
        this.p = new paper.Path();
        this.p.strokeColor = 'green';
        this.p.strokeColor.alpha = 0.3;

        this.g.addChild(this.p);
        paper.view.draw();
    }

    reset(rob, points, radius) {
        radius = this.drawing.toValue(radius);

        // Remove all children but the path
        this.g.removeChildren(1);

        this.p.segments = [];
        this.p.strokeWidth = 2 * radius;

        let pt = this.drawing.toPoint(rob);
        this.p.add(pt);

        points.forEach((point) => {
            pt = this.drawing.toPoint(point);
            this.p.add(pt);

            let step = new paper.Path.Circle(pt, radius);
            step.fillColor = 'green';
            step.fillColor .alpha = 0.8;
            this.g.addChild(step);
        });

        paper.view.draw();
    }
}
/* ******************************** */

let move = null;
exports.show = function() {
    var content = $("#content");
    content.empty();

    move = new Move(content);
};

exports.host = "";

exports.stop = function() {
    move.stop();

    var content = $("#content");
    content.empty();
};
