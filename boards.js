var $ = require("jquery");

var base = require('./boards/base.js');
var vlx = require("./boards/vlx.js");
var servos = require("./boards/servos.js");
var futabas = require("./boards/futabas.js");
var pumps = require("./boards/pumps.js");
var motor = require("./boards/motor.js");

var pannels = {
    servos: servos,
    futabas: futabas,
    vlx: vlx,
    pumps: pumps,
    motor: motor,
};

exports.show = function() {
    var content = $("#content");
    content.empty();
    base.host = exports.host;

    $.get('http://' + exports.host + ':8080/api/boards', function(data) {
        $.each(data, function(ix, val) {
            if (pannels.hasOwnProperty(val.type)) {
                var board = new pannels[val.type].Board(val);
                content.append(board.div);
            } else {
                console.log("Unknown board ", val);
            }
        });
    });
};

exports.host = "";

exports.stop = function() {
    var content = $("#content");
    content.empty();
};
